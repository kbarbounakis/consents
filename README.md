# @universis/consents

A collection of tools and services for managing user consents.

## Installation

```bash
npm i @universis/consents
```

## Configuration

Register `UserConsentService` as application service:

```json
{
  "services": [
    {
      "serviceType": "@universis/consents#UserConsentService"
    }
  ]
}
```

and `UserConsentSchemaLoader` under `settings/schema/loaders`:

```json
{
  "settings": {
    "schema": {
      "loaders": [
        {
          "loaderType": "@universis/consents#UserConsentSchemaLoader"
        }
      ]
    }
  }
}
```

`UserConsentSchemaLoader` consists of a collection of data models that are going to be used by  application services for controlling user consents.

## Description

This module provides a set of tools and services for managing user consents. It allows you to create, update, delete and retrieve consents. It also provides a set of tools for managing user consents in the application.

### ConsentField

User consents fields are stored in the database and are associated with a user. Each consent field has a unique identifier, a name, a description, a status, a creation date and an update date etc. A consent may be archived or active. An archived consent is a consent that is no longer in use. An active consent is a consent that is currently in use.

```json
{
  "name": "Allow contact",
  "alternateName": "allowContact",
  "description": "Allow the institution to contact me via email",
  "group": "students",
  "id": "e82f925b-28bc-487a-b517-31f014885e4d",
  "archived": false,
  "additionalType": "ConsentField",
  "dateCreated": "2024-03-21T15:38:29.351Z",
  "dateModified": "2024-03-21T15:38:29.353Z"
}
```

### ConsentFieldGroup

Each consent field may belong to a group of consents. A consent group is a collection of consents that are related to each other. A consent group has a unique identifier, a name, a description, a creation date and an update date etc

```json
{
  "id": "93703531-795d-41d7-b4de-4cc1fe940809",
  "name": "Student Consents",
  "alternateName": "students",
  "archived": false,
  "additionalType": "ConsentFieldGroup",
  "description": "A consent group for students",
  "dateCreated": "2024-03-21T15:41:12.917Z",
  "dateModified": "2024-03-21T15:41:12.928Z"
}
```

### UserConsents

User consents can be retrieved by the user. The user can retrieve all their consents by calling `/api/users/me/consents` endpoint.

The list of user consents is being represented as a list of consent objects.

```json
{
  "students": {
    "allowContact": {
      "val": "y",
      "metadata": {
        "time": "2024-03-21T15:29:56.258Z",
        "name": "Allow contact",
        "description": "Allow the institution to contact me via email",
      }
    },
    "sms": {
      "val": "u",
      "metadata": {
        "time": null,
        "name": "Allow sending SMS",
        "description": "Allow the institution to contact me via sms",
      }
    },
    "metadata": {
      "time": "2024-03-21T15:29:56.258Z",
      "name": "Student Consents",
      "description": "A consent group for students",
    }
  }
}
```
User consents can be updated by the user. The user can update their consents by calling `/api/users/me/consents` endpoint.

```json
{
  "students": {
    "allowContact": {
      "val": "n"
    },
    "sms": {
      "val": "n"
    }
  }
}
```

