
import { DataCacheStrategy, DefaultDataCacheStrategy, ODataModelBuilder, SchemaLoaderStrategy } from '@themost/data';
import { getApplication, serveApplication, getToken as authenticate, getServerAddress  } from '@themost/test'
import { UserConsentSchemaLoader, UserConsentService } from '@universis/consents';
import { BasicDataContext } from '@themost/client/common';
import { Router } from 'express';
import { DefaultScopeAccessConfiguration, ScopeAccessConfiguration, validateScope } from '@universis/janitor';

describe('UserConsentService', () => {

    /**
     * @type {string}
     */
    let serverAddress;
    /**
     * @type {import('@themost/express').ExpressDataContext}
     */
    let context;
    beforeAll(async () => {
        // get application container
        const container = getApplication();
        /**
         * @type {import('@themost/express').ExpressDataApplication}
         */
        const app = container.get('ExpressDataApplication');
        app.getConfiguration().useStrategy(ScopeAccessConfiguration, DefaultScopeAccessConfiguration)
        const scopeAccess = app.getConfiguration().getStrategy(ScopeAccessConfiguration);
        /* eslint-disable quotes */
        scopeAccess.elements.push({
            "scope": [
                "registrar"
            ],
            "resource": "/api/",
            "access": [
                "read",
                "write"
            ]
        }, {
            "scope": [
                "profile"
            ],
            "resource": "/api/users/me",
            "access": [
                "read"
            ]
        });
        /* eslint-enable quotes */
        app.useService(UserConsentService);

        app.serviceRouter.subscribe((serviceRouter) => {
            const addRouter = Router();
            addRouter.use(validateScope());
            addRouter.use((req, res, next) => {
                req.context.culture = () => {
                    if (req.headers['accept-language']) {
                        return req.headers['accept-language'].split(',')[0];
                    }
                    return 'en';
                };
                req.context.locale = req.context.culture();
                return next();
            });
            serviceRouter.stack.unshift.apply(serviceRouter.stack, addRouter.stack); 
        });
        const configuration = app.getConfiguration();
        configuration.setSourceAt('settings/i18n/locales', [
            'en',
            'el'
        ]);

        // upgrade caching
        const service = configuration.getStrategy(DataCacheStrategy);
        if (service instanceof DefaultDataCacheStrategy) {
            const superRemove = DefaultDataCacheStrategy.prototype.remove;
            service.remove = async (key) => {
                if (key.indexOf('*') >= 0) {
                    // try to find all keys and remove them
                    const keys = service.rawCache.keys();
                    const search = key.replace('*', '.*');
                    const re = new RegExp(search);
                    const found = keys.filter((x) => re.test(x));
                    for(const k of found) {
                        await superRemove.call(service, k);
                    }
                }
                else {
                    await superRemove.call(service, key);
                }
            }
        }

        /**
         * get schema loader strategy
         * @type {import('@themost/data').DefaultSchemaLoaderStrategy}
         */
        const schema = configuration.getStrategy(SchemaLoaderStrategy);
        // push user consent schema loader
        schema.loaders.push(new UserConsentSchemaLoader(configuration));
        // cleanup model builder
        app.getService(ODataModelBuilder).clean(true);
        // get context
        context = app.createContext();
        context.culture = () => 'en';
        context.locale = 'en';
        const server = await serveApplication(container, 7000);
        serverAddress = getServerAddress(server);
    });

    it('should create instance', () => {
        const service = context.application.getService(UserConsentService);
        expect(service).toBeTruthy();
        const model = context.model('ConsentField');
        expect(model).toBeTruthy();
    });

    it('should create consent field group', async () => {
        const service = context.application.getService(UserConsentService);
        expect(service).toBeTruthy();
        const ConsentFieldGroups = context.model('ConsentFieldGroup').silent(); 
        const newGroup = {
            name: 'Student Consents',
            alternateName: 'students',
            description: 'A consent group for students',
            locales: [
                {
                    name: 'Συναινέσεις Φοιτητών',
                    description: 'Μία ομάδα συναινέσεων για τους φοιτητές του ιδρύματος',
                    inLanguage: 'el',
                }
            ]
        };
        await ConsentFieldGroups.save(newGroup);
        const item = await ConsentFieldGroups.where('name').equal('Student Consents').expand('locales').getItem();
        expect(item).toBeTruthy();
        expect(item.locale).toBeTruthy();
        const locale = item.locales.find((x) => x.inLanguage === 'el');
        expect(locale).toBeTruthy();
        expect(locale.name).toEqual('Συναινέσεις Φοιτητών');
    });

    it('should create consent field', async () => {
        const ConsentFields = context.model('ConsentField').silent(); 
        const ConsentFieldGroups = context.model('ConsentFieldGroup').silent(); 
        const newField = {
            name: 'Allow contact',
            alternateName: 'allowContact',
            description: 'Allow the institution to contact me via email',
            group: 'students',
            locales: [
                {
                    name: 'Συναίνεση επικοινωνίας',
                    description: 'Να επιτρέπεται στο ιδρυμα να επικοινωνεί μαζί μου μέσω email',
                    inLanguage: 'el',
                }
            ]
        };
        await ConsentFields.save(newField);
        // add another field
        await ConsentFields.save({
            name: 'Allow sending SMS',
            alternateName: 'sms',
            description: 'Allow the institution to contact me via sms',
            group: 'students',
            locales: [
                {
                    name: 'Συναίνεση αποστολής SMS',
                    description: 'Να επιτρέπεται στο ιδρυμα να επικοινωνεί μαζί μου μέσω sms',
                    inLanguage: 'el',
                }
            ]
        });
        const item = await ConsentFields.where('name').equal('Allow contact').expand('locales').getItem();
        expect(item).toBeTruthy();
        // try to change alternate name
        const group = await ConsentFieldGroups.where('name').equal('Student Consents').expand('locales').getItem();
        group.alternateName = 'students1';
        await ConsentFieldGroups.save(group);
        let items = await ConsentFields.where('group').equal('students1').getItems();
        expect(items).toBeTruthy();
        expect(items.length).toBeTruthy();
        group.alternateName = 'students';
        await ConsentFieldGroups.save(group);
        items = await ConsentFields.where('group').equal('students').getItems();
        expect(items).toBeTruthy();
        expect(items.length).toBeTruthy();
    });

    it('should add user consent', async () => {
        const UserConsents = context.model('UserConsent').silent(); 
        const newItem = {
            consent: 'allowContact',
            value: 'y'
        };
        context.user = {
            name: 'alexis.rees@example.com'
        };
        await UserConsents.save(newItem);
        const item = await UserConsents.where('consent').equal('allowContact').getItem();
        expect(item).toBeTruthy();
        expect(item.value).toEqual('y');
        // get user consents
        const values = await context.application.getService(UserConsentService).getConsentsForCurrentUser(context);
        expect(values).toBeTruthy();
        expect(values.students).toBeTruthy();
        expect(values.students.allowContact).toBeTruthy();
        expect(values.students.allowContact.val).toEqual('y');
    });

    it('should get user consents', async () => {
        // use token to authenticate
        const { access_token } = await authenticate(serverAddress, 'lucas.richards@example.com', 'secret');
        const context = new BasicDataContext(new URL('/api/', serverAddress));
        context.setBearerAuthorization(access_token);
        let items = await context.model('users/me/consents').asQueryable().getItems();
        expect(items).toBeTruthy();
        context.getService().setHeader('Accept-Language', 'el');
        items = await context.model('users/me/consents').asQueryable().getItems();
        expect(items).toBeTruthy();
        items.value.students.allowContact.val = 'n';
        items.value.students.sms.val = 'y';
        await context.model('users/me/consents').save(items.value);
        items = await context.model('users/me/consents').asQueryable().getItems();
        expect(items).toBeTruthy();
        expect(items.value.students.allowContact.val).toEqual('n');
        expect(items.value.students.sms.val).toEqual('y');
    });

    it('should get user consent group', async () => {
        const { access_token } = await authenticate(serverAddress, 'lucas.richards@example.com', 'secret');
        const context = new BasicDataContext(new URL('/api/', serverAddress));
        context.setBearerAuthorization(access_token);
        let result = await context.model('users/me/consents/students').asQueryable().getItem();
        expect(result.value).toBeTruthy();
        const keys = Object.keys(result.value);
        expect(keys.length).toEqual(1);
        expect(result.value.students).toBeTruthy();
        expect(result.value.students.allowContact.val).toEqual('n');
        await expectAsync(context.model('users/me/consents/missing').asQueryable().getItem()).toBeRejectedWithError('The specified consent group cannot be found or is inaccessible');
        await expectAsync(context.model('users/me/consents/mis(sing').asQueryable().getItem()).toBeRejectedWithError('Consent group name is not valid');

    });

    it('should set user consent group', async () => {
        const { access_token } = await authenticate(serverAddress, 'lucas.richards@example.com', 'secret');
        const context = new BasicDataContext(new URL('/api/', serverAddress));
        context.setBearerAuthorization(access_token);
        let result = await context.model('users/me/consents/students').execute({
            'students': {
              'allowContact': {
                'val': 'n'
              },
              'sms': {
                'val': 'n'
              }
            }
          });
        expect(result.value).toBeTruthy();
        result = await context.model('users/me/consents/students').asQueryable().getItem();
        expect(result.value).toBeTruthy();
        expect(result.value.students.allowContact.val).toEqual('n');
    });
    
});