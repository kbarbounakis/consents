import { DataObject, EdmMapping } from '@themost/data';

@EdmMapping.entityType('ConsentFieldGroup')
class ConsentFieldGroup extends DataObject {
    constructor() {
        super();
    }
}

export {
    ConsentFieldGroup
}