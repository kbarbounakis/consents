import { DataObject } from '@themost/data';
import { ConsentField } from './ConsentField';
export declare class ConsentFieldGroup extends DataObject {
    id?: string;
    name?: string;
    alternateName?: string;
    description?: string;
    url?: string;
    image?: string;
    archived?: boolean;
    dateCreated?: Date;
    dateModified?: Date;
    createdBy?: any;
    modifiedBy?: any;
    fields?: ConsentField[];
    locale?: {
        name?: string,
        description?: string
    };
    locales?: {
        name?: string,
        description?: string,
    }[];
}