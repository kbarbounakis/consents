
class GenericConsentField {

    val = 'u';
    metadata = {
        time: null,
        id: null,
        name: null,
        description: null,
        archived: false,
        url: null,
        expiration: null
    }
    
}

export {
    GenericConsentField
}